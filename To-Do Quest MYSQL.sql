-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Fri May 22 15:53:50 2020 
-- * LUN file: E:\Programmazione\dbms\Progetto\To-Do Quest.lun 
-- * Schema: Rel To-Do Quest/1-1 
-- ********************************************* 


-- Database Section
-- ________________ 
DROP DATABASE IF EXISTS `To-Do Quest`;
CREATE DATABASE `To-Do Quest`;
USE `To-Do Quest`;


-- Tables Section
-- _____________ 

CREATE TABLE AcquistiCosmetico (
    NomeCosmetico VARCHAR(80) NOT NULL,
    UsernameUtente VARCHAR(80) NOT NULL,
    NomePersonaggio VARCHAR(80) NOT NULL,
    CONSTRAINT IDAcquistoCosmetico PRIMARY KEY (NomeCosmetico , UsernameUtente , NomePersonaggio)
);

CREATE TABLE Ambientazioni (
    NomeAmbientazione VARCHAR(80) NOT NULL,
    Descrizione VARCHAR(500) NOT NULL,
    CONSTRAINT IDAmbientazioneDungeon PRIMARY KEY (NomeAmbientazione)
);

CREATE TABLE AppartenenzeCategoria (
    UsernameUtente VARCHAR(80) NOT NULL,
    NomeCategoria VARCHAR(80) NOT NULL,
    IDMainQuest INT NOT NULL,
    CONSTRAINT IDAppartenenzaCategoria PRIMARY KEY (UsernameUtente , NomeCategoria , IDMainQuest)
);

CREATE TABLE Aspetti (
    NomeAspetto VARCHAR(80) NOT NULL,
    Descrizione VARCHAR(500) NOT NULL,
    CONSTRAINT IDPreSet PRIMARY KEY (NomeAspetto)
);

CREATE TABLE CategorieQuest (
    UsernameUtente VARCHAR(80) NOT NULL,
    NomeCategoria VARCHAR(80) NOT NULL,
    CONSTRAINT IDCategoriaQuest PRIMARY KEY (UsernameUtente , NomeCategoria)
);

CREATE TABLE Classi (
    NomeClasse VARCHAR(80) NOT NULL,
    Descrizione VARCHAR(500) NOT NULL,
    CONSTRAINT IDClasse PRIMARY KEY (NomeClasse)
);

CREATE TABLE Cosmetici (
    Prezzo INT NOT NULL,
    NomeCosmetico VARCHAR(80) NOT NULL,
    Descrizione VARCHAR(500) NOT NULL,
    Tipo VARCHAR(80) NOT NULL,
    CONSTRAINT IDCosmetico PRIMARY KEY (NomeCosmetico)
);

CREATE TABLE Difficoltà (
    LivelloDifficoltà INT NOT NULL,
    RicompensaGemme INT NOT NULL,
    RicompensaXP INT NOT NULL,
    CONSTRAINT IDDifficoltà PRIMARY KEY (LivelloDifficoltà)
);

CREATE TABLE Dungeon (
    UsernameUtente VARCHAR(80) NOT NULL,
    NomeDungeon VARCHAR(80) NOT NULL,
    IDDungeon INT NOT NULL,
    IDGradoSfida INT NOT NULL,
    NomeAmbientazione VARCHAR(80) NOT NULL,
    CONSTRAINT IDDungeon_ID PRIMARY KEY (UsernameUtente , IDDungeon)
);

CREATE TABLE GradiSfida (
    IDGradoSfida INT NOT NULL,
    RicompensaGemme INT NOT NULL,
    PenalitàGemme INT NOT NULL,
    CONSTRAINT IDDifficoltàDungeon PRIMARY KEY (IDGradoSfida)
);

CREATE TABLE MainQuest (
    IDMainQuest INT NOT NULL AUTO_INCREMENT,
    UsernameUtente VARCHAR(80) NOT NULL,
    NomePersonaggio VARCHAR(80) NOT NULL,
    Nome VARCHAR(80) NOT NULL,
    DataCreazione DATE NOT NULL,
    Completato CHAR NOT NULL,
    Descrizione VARCHAR(500),
    DataObiettivo DATE,
    LivelloDifficoltà INT NOT NULL,
    CONSTRAINT IDMainQuest PRIMARY KEY (IDMainQuest)
);

CREATE TABLE Nemici (
    IDNemico INT NOT NULL AUTO_INCREMENT,
    Nome VARCHAR(80) NOT NULL,
    Descrizione VARCHAR(500) NOT NULL,
    Livello INT NOT NULL,
    CONSTRAINT IDNemico PRIMARY KEY (IDNemico)
);

CREATE TABLE Personaggi (
    UsernameUtente VARCHAR(80) NOT NULL,
    NomePersonaggio VARCHAR(80) NOT NULL,
    PuntiXP INT NOT NULL,
    Gemme INT NOT NULL,
    NomeAspetto VARCHAR(80) NOT NULL,
    NomeClasse VARCHAR(80) NOT NULL,
    EquipaggiamentoTesta VARCHAR(80),
    EquipaggiamentoCorpo VARCHAR(80),
    EquipaggiamentoGambe VARCHAR(80),
    CONSTRAINT IDPersonaggio PRIMARY KEY (UsernameUtente , NomePersonaggio)
);

CREATE TABLE PopolazioneDungeon (
	IDPopolazione INT NOT NULL AUTO_INCREMENT,
    UsernameUtente VARCHAR(80) NOT NULL,
    IDDungeon INT NOT NULL,
    IDNemico INT NOT NULL,
    CONSTRAINT IDPopolazioneDungeon PRIMARY KEY (IDPopolazione)
);

CREATE TABLE SubQuest (
    IDMainQuest INT NOT NULL,
    Nome VARCHAR(80) NOT NULL,
    DataCreazione DATE NOT NULL,
    Completato CHAR NOT NULL,
    Descrizione VARCHAR(500),
    DataObiettivo DATE,
    CONSTRAINT IDSubQuest PRIMARY KEY (IDMainQuest , Nome)
);

CREATE TABLE SvantaggiAmbientazione (
    NomeAmbientazione VARCHAR(80) NOT NULL,
    NomeClasse VARCHAR(80) NOT NULL,
    CONSTRAINT IDSvantaggioAmbientazione PRIMARY KEY (NomeAmbientazione , NomeClasse)
);

CREATE TABLE Utenti (
    UsernameUtente VARCHAR(80) NOT NULL,
    Nome VARCHAR(80) NOT NULL,
    Cognome VARCHAR(80) NOT NULL,
    CONSTRAINT IDUtente PRIMARY KEY (UsernameUtente)
);

CREATE TABLE VantaggiAmbientazione (
    NomeClasse VARCHAR(80) NOT NULL,
    NomeAmbientazione VARCHAR(80) NOT NULL,
    CONSTRAINT IDVantaggioAmbientazione PRIMARY KEY (NomeAmbientazione , NomeClasse)
);


-- Constraints Section
-- ___________________ 

ALTER TABLE AcquistiCosmetico ADD CONSTRAINT FKAcq_Per
     FOREIGN KEY (UsernameUtente, NomePersonaggio)
     REFERENCES Personaggi (UsernameUtente, NomePersonaggio);

ALTER TABLE AcquistiCosmetico ADD CONSTRAINT FKAcq_Cos
     FOREIGN KEY (NomeCosmetico)
     REFERENCES Cosmetici (NomeCosmetico);

ALTER TABLE AppartenenzeCategoria ADD CONSTRAINT FKAC_MainQuest
     FOREIGN KEY (IDMainQuest)
     REFERENCES MainQuest (IDMainQuest);

ALTER TABLE AppartenenzeCategoria ADD CONSTRAINT FKAC_CategoriaQuest
     FOREIGN KEY (UsernameUtente, NomeCategoria)
     REFERENCES CategorieQuest (UsernameUtente, NomeCategoria);

ALTER TABLE CategorieQuest ADD CONSTRAINT FKCreazioneCategoria
     FOREIGN KEY (UsernameUtente)
     REFERENCES Utenti (UsernameUtente);

-- Not implemented
-- alter table Dungeon add constraint IDDungeon_CHK
--     check(exists(select * from PopolazioneDungeon
--                  where PopolazioneDungeon.UsernameUtente = UsernameUtente and PopolazioneDungeon.IDDungeon = IDDungeon)); 

ALTER TABLE Dungeon ADD CONSTRAINT FKGradoSfidaDungeon
     FOREIGN KEY (IDGradoSfida)
     REFERENCES GradiSfida (IDGradoSfida);

ALTER TABLE Dungeon ADD CONSTRAINT FKAmbientazioneDungeon
     FOREIGN KEY (NomeAmbientazione)
     REFERENCES Ambientazioni (NomeAmbientazione);

ALTER TABLE Dungeon ADD CONSTRAINT FKGenerazioneDungeon
     FOREIGN KEY (UsernameUtente)
     REFERENCES Utenti (UsernameUtente);

ALTER TABLE MainQuest ADD CONSTRAINT FKDifficoltàQuest
     FOREIGN KEY (LivelloDifficoltà)
     REFERENCES Difficoltà (LivelloDifficoltà);

ALTER TABLE MainQuest ADD CONSTRAINT FKAffrontaQuest
     FOREIGN KEY (UsernameUtente, NomePersonaggio)
     REFERENCES Personaggi (UsernameUtente, NomePersonaggio);

ALTER TABLE Personaggi ADD CONSTRAINT FKPossiedeAspetto
     FOREIGN KEY (NomeAspetto)
     REFERENCES Aspetti (NomeAspetto);

ALTER TABLE Personaggi ADD CONSTRAINT FKAppartenenzaClasse
     FOREIGN KEY (NomeClasse)
     REFERENCES Classi (NomeClasse);

ALTER TABLE Personaggi ADD CONSTRAINT FKCreazionePersonaggio
     FOREIGN KEY (UsernameUtente)
     REFERENCES Utenti (UsernameUtente);

ALTER TABLE Personaggi ADD CONSTRAINT FKEquipaggiamentoTesta
     FOREIGN KEY (EquipaggiamentoTesta)
     REFERENCES Cosmetici (NomeCosmetico);

ALTER TABLE Personaggi ADD CONSTRAINT FKEquipaggiamentoCorpo
     FOREIGN KEY (EquipaggiamentoCorpo)
     REFERENCES Cosmetici (NomeCosmetico);

ALTER TABLE Personaggi ADD CONSTRAINT FKEquipaggiamentoGambe
     FOREIGN KEY (EquipaggiamentoGambe)
     REFERENCES Cosmetici (NomeCosmetico);

ALTER TABLE PopolazioneDungeon ADD CONSTRAINT FKDungeonPopolazione
     FOREIGN KEY (UsernameUtente, IDDungeon)
     REFERENCES Dungeon (UsernameUtente, IDDungeon);

ALTER TABLE PopolazioneDungeon ADD CONSTRAINT FKR
     FOREIGN KEY (IDNemico)
     REFERENCES Nemici (IDNemico);

ALTER TABLE SubQuest ADD CONSTRAINT FKAppartieneQuest
     FOREIGN KEY (IDMainQuest)
     REFERENCES MainQuest (IDMainQuest);

ALTER TABLE SvantaggiAmbientazione ADD CONSTRAINT FKSA_Classe
     FOREIGN KEY (NomeClasse)
     REFERENCES Classi (NomeClasse);

ALTER TABLE SvantaggiAmbientazione ADD CONSTRAINT FKSA_Ambientazione
     FOREIGN KEY (NomeAmbientazione)
     REFERENCES Ambientazioni (NomeAmbientazione);

ALTER TABLE VantaggiAmbientazione ADD CONSTRAINT FKVan_Amb
     FOREIGN KEY (NomeAmbientazione)
     REFERENCES Ambientazioni (NomeAmbientazione);

ALTER TABLE VantaggiAmbientazione ADD CONSTRAINT FKVan_Cla
     FOREIGN KEY (NomeClasse)
     REFERENCES Classi (NomeClasse);


-- Setup Section
-- _____________ 

INSERT INTO aspetti VALUES("Orco", "Un orco delle paludi. Basso e tarchiato.");
INSERT INTO aspetti VALUES("Elfo", "Un elfo degli altopiani. Slanciato e pallido.");
INSERT INTO aspetti VALUES("Umano", "Un umano, proveniente dalla città.");
INSERT INTO aspetti VALUES("Nano", "Un nano, proveniente dalle montagne.");
INSERT INTO aspetti VALUES("Mezz'elfo", "Un mezz'elfo, proveniente dalla capitale.");

INSERT INTO classi VALUES("Bardo", "Un musicante che vaga di città in città.");
INSERT INTO classi VALUES("Mago", "Un pericoloso allievo della arti oscure");
INSERT INTO classi VALUES("Ranger", "Da sempre cresciuto immerso nella natura, è  a suo agio nella foresta.");
INSERT INTO classi VALUES("Paladino", "Guerriero senza paura, combatte con scudo e spada");
INSERT INTO classi VALUES("Ladro", "Silenzioso e letale, si aggira di soppiatto");

INSERT INTO difficoltà VALUES(1, 10, 50);
INSERT INTO difficoltà VALUES(2, 20, 100);
INSERT INTO difficoltà VALUES(3, 30, 150);
INSERT INTO difficoltà VALUES(4, 40, 200);
INSERT INTO difficoltà VALUES(5, 50, 250);
INSERT INTO difficoltà VALUES(10, 10, 500);
INSERT INTO difficoltà VALUES(15, 15, 750);
INSERT INTO difficoltà VALUES(20, 20, 1000);
INSERT INTO difficoltà VALUES(25, 25, 1250);

INSERT INTO ambientazioni VALUES("Foresta", "Un'oscura foresta.");
INSERT INTO ambientazioni VALUES("Miniera", "Un'antica miniera dei nani.");
INSERT INTO ambientazioni VALUES("Labirinto", "Un labirinto costruito da uno stregone.");
INSERT INTO ambientazioni VALUES("Deserto", "Un deserto sconfinato.");
INSERT INTO ambientazioni VALUES("Montagna", "Una montagna alta e ripida.");

INSERT INTO gradisfida VALUES(1, 5, 7);
INSERT INTO gradisfida VALUES(3, 15, 20);
INSERT INTO gradisfida VALUES(5, 25, 34);
INSERT INTO gradisfida VALUES(7, 35, 48);
INSERT INTO gradisfida VALUES(11, 55, 66);
INSERT INTO gradisfida VALUES(17, 75, 88);
INSERT INTO gradisfida VALUES(21, 87, 100);

INSERT INTO nemici(Nome, Descrizione, Livello) VALUES("Zombie", "Un cadavere in putrefazione, rianimato dall'arte oscura", 1);
INSERT INTO nemici(Nome, Descrizione, Livello) VALUES("Goblin", "Un goblin, perlopiù innocuo", 2);
INSERT INTO nemici(Nome, Descrizione, Livello) VALUES("Coboldo", "Un viscido coboldo", 4);
INSERT INTO nemici(Nome, Descrizione, Livello) VALUES("Bugbear", "Un goblin troppo cresciuto", 6);
INSERT INTO nemici(Nome, Descrizione, Livello) VALUES("Orsogufo", "Una bestia spaventosa", 10);
INSERT INTO nemici(Nome, Descrizione, Livello) VALUES("Gladiatore", "Un combattente esperto", 15);

INSERT INTO vantaggiambientazione VALUES("Ranger", "Foresta");
INSERT INTO vantaggiambientazione VALUES("Mago", "Miniera");
INSERT INTO vantaggiambientazione VALUES("Paladino", "Deserto");
INSERT INTO vantaggiambientazione VALUES("Ladro", "Montagna");

INSERT INTO svantaggiambientazione VALUES("Labirinto", "Mago");
INSERT INTO svantaggiambientazione VALUES("Foresta", "Bardo");
INSERT INTO svantaggiambientazione VALUES("Deserto", "Ladro");
INSERT INTO svantaggiambientazione VALUES("Miniera", "Bardo");

INSERT INTO cosmetici VALUES(10, "Elmo di ferro", "Un robusto elmo di ferro", "Testa");
INSERT INTO cosmetici VALUES(20, "Cappello appariscente", "Un largo cappello, probabilmente appartenuto ad una ricca signora", "Testa");

INSERT INTO cosmetici VALUES(7, "Giacca di pelle", "Una giacca di pelle alla moda", "Corpo");
INSERT INTO cosmetici VALUES(43, "Tunica da stregone", "Una tunica appartenuta ad un potente stregone. O forse è un falso?", "Corpo");

INSERT INTO cosmetici VALUES(55, "Scarponi da montagna", "Robusti e affidabili", "Gambe");
INSERT INTO cosmetici VALUES(157, "Zoccoli d'avorio", "Preziosi zoccoli d'avorio, rari sul mercato", "Gambe");

INSERT INTO cosmetici VALUES(46, "Elmo d'oro", "Elmo per veri guerrieri", "Testa");
INSERT INTO cosmetici VALUES(89, "Stivali di ferro", "Robusti stivali in ferro resistente", "Gambe");

INSERT INTO cosmetici VALUES(123, "Armatura di catene", "Leggera e robusta armatura di catene", "Corpo");
INSERT INTO cosmetici VALUES(50, "Placca d'acciaio", "Placca di un acciaio luccicante", "Corpo");