package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.apache.commons.dbutils.QueryRunner;

import it.todoquest.db.Characters;
import it.todoquest.db.Cosmetics;
import it.todoquest.model.Cosmetic;

public class ViewCharacter {
	it.todoquest.model.Character character;
	final QueryRunner qr;

	final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	final int screenWidth = screenSize.width - (screenSize.width / 7);
	final int screenHeight = screenSize.height - (screenSize.height / 7);

	public ViewCharacter(final it.todoquest.model.Character character, final QueryRunner qr) {
		this.character = character;
		this.qr = qr;
		try {
			this.initView();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void initView() throws SQLException {
		// Creating the Frame
		JFrame frame = new JFrame(this.character.getNomePersonaggio());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Setting the Sizes according to the screen
		frame.setSize(screenWidth, screenHeight);

		// Character Section - Displays Name and Image
		// Creating the panel for the first column
		JPanel characterPanel = new JPanel();
		characterPanel.setLayout(new BoxLayout(characterPanel, BoxLayout.PAGE_AXIS));
		characterPanel.setPreferredSize(new Dimension((screenWidth / 3) - 20, screenHeight - 50));
		// Creating the title for the first column
		final JLabel characterLabel = new JLabel(this.character.getNomePersonaggio(), JLabel.CENTER);
		characterLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
		characterPanel.add(characterLabel);

		final JLabel aspettoLabel = new JLabel(
				"<html><br/><b>Aspetto: </b>" + this.character.getNomeAspetto() + "</html>", JLabel.LEFT);
		aspettoLabel.setFont(new Font("Helvetica", Font.PLAIN, 36));
		characterPanel.add(aspettoLabel);

		final JLabel classeLabel = new JLabel("<html><b>Classe: </b>" + this.character.getNomeClasse() + "</html>",
				JLabel.LEFT);
		classeLabel.setFont(new Font("Helvetica", Font.PLAIN, 36));
		characterPanel.add(classeLabel);

		final JLabel gemsLabel = new JLabel("<html><b>Gemme: </b>" + this.character.getGemme() + "</html>",
				JLabel.LEFT);
		gemsLabel.setFont(new Font("Helvetica", Font.PLAIN, 36));
		characterPanel.add(gemsLabel);

		final JLabel xpLabel = new JLabel("<html><b>XP: </b>" + this.character.getPuntiXP() + "</html>", JLabel.LEFT);
		xpLabel.setFont(new Font("Helvetica", Font.PLAIN, 36));
		characterPanel.add(xpLabel);

		String equipTesta;
		if (this.character.getEquipaggiamentoTesta() == null) {
			equipTesta = "Niente";
		} else {
			equipTesta = this.character.getEquipaggiamentoTesta();
		}
		final JLabel testaLabel = new JLabel("<html><b>Equipaggiamento Testa: </b><br/>" + equipTesta + "</html>",
				JLabel.LEFT);
		testaLabel.setFont(new Font("Helvetica", Font.PLAIN, 24));
		characterPanel.add(testaLabel);

		String equipCorpo;
		if (this.character.getEquipaggiamentoCorpo() == null) {
			equipCorpo = "Niente";
		} else {
			equipCorpo = this.character.getEquipaggiamentoCorpo();
		}
		final JLabel corpoLabel = new JLabel("<html><b>Equipaggiamento Corpo: </b><br/>" + equipCorpo + "</html>",
				JLabel.LEFT);
		corpoLabel.setFont(new Font("Helvetica", Font.PLAIN, 24));
		characterPanel.add(corpoLabel);

		String equipGambe;
		if (this.character.getEquipaggiamentoGambe() == null) {
			equipGambe = "Niente";
		} else {
			equipGambe = this.character.getEquipaggiamentoGambe();
		}
		final JLabel gambeLabel = new JLabel("<html><b>Equipaggiamento Gambe: </b><br/>" + equipGambe + "</html>",
				JLabel.LEFT);
		gambeLabel.setFont(new Font("Helvetica", Font.PLAIN, 24));
		characterPanel.add(gambeLabel);

		// Shop and Inventory Section
		// Creating the panel for the second column
		JPanel itemPanel = new JPanel();
		itemPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
		itemPanel.setPreferredSize(new Dimension((screenWidth / 3) + (screenWidth / 3) - 20, screenHeight - 50));
		// Creating the title for the second column
		final JLabel inventoryLabel = new JLabel("Inventario", JLabel.CENTER);
		inventoryLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
		itemPanel.add(inventoryLabel);

		// Creating the list of buttons for the iventory
		List<Cosmetic> inventoryList = null;
		try {
			inventoryList = Cosmetics.getPurchases(qr, character);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		JPanel inventoryButtonsPanel = new JPanel();
		inventoryButtonsPanel.setLayout(new BoxLayout(inventoryButtonsPanel, BoxLayout.Y_AXIS));
		inventoryList.stream().map(i -> this.createInventoryButton(i, frame)).forEach(inventoryButtonsPanel::add);

		// Creating the ScrollPane for the buttons
		JScrollPane inventoryButtonsScroll = new JScrollPane(inventoryButtonsPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		inventoryButtonsScroll.setPreferredSize(
				new Dimension((screenWidth / 3) + (screenWidth / 3) - 50, (screenHeight / 2) - (screenHeight / 8)));
		itemPanel.add(inventoryButtonsScroll);

		// Creating the title for the second column
		final JLabel shopTextLabel = new JLabel("Negozio", JLabel.CENTER);
		shopTextLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
		itemPanel.add(shopTextLabel);

		// Creating the list of buttons for the iventory
		List<Cosmetic> shopList = null;
		try {
			shopList = Cosmetics.getStoreEntries(character, qr);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		JPanel shopButtonsPanel = new JPanel();
		shopButtonsPanel.setLayout(new BoxLayout(shopButtonsPanel, BoxLayout.Y_AXIS));

		shopList.stream().map(e -> this.createShopButton(e, frame)).forEach(shopButtonsPanel::add);

		// Creating the ScrollPane for the buttons
		JScrollPane shopButtonsScroll = new JScrollPane(shopButtonsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		shopButtonsScroll.setPreferredSize(
				new Dimension((screenWidth / 3) + (screenWidth / 3) - 50, (screenHeight / 2) - (screenHeight / 8)));
		itemPanel.add(shopButtonsScroll);

		// Adding Components to the frame.
		frame.getContentPane().add(BorderLayout.WEST, characterPanel);
		frame.getContentPane().add(BorderLayout.EAST, itemPanel);
		frame.setVisible(true);
		frame.setResizable(false);

		/* Some piece of code */
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				new ViewMainMenu(qr, character.getUsernameUtente());
			}
		});
	}

	private JComponent createInventoryButton(Cosmetic cosmetic, JFrame frame) {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 10, 5, 10));
		panel.setLayout(new BorderLayout());
		panel.setMaximumSize(new Dimension((this.screenWidth / 3) * 2 - 20, this.screenHeight / 10));

		JPanel infos = new JPanel();
		infos.setLayout(new GridLayout(3, 1, 0, 5));

		JLabel name = new JLabel(cosmetic.getNomeCosmetico());
		name.setFont(new Font("Helvetica", Font.BOLD, 18));
		infos.add(name);
		JLabel gems = new JLabel("Descrizione: " + cosmetic.getDescrizione());
		gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
		infos.add(gems);
		JLabel xp = new JLabel("Tipo: " + cosmetic.getTipo());
		xp.setFont(new Font("Helvetica", Font.PLAIN, 18));
		infos.add(xp);

		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(1, 1, 0, 0));

		JButton button = new JButton("Equipaggia Cosmetico");
		button.setFont(new Font("Helvetica", Font.BOLD, 18));
		if (character.getEquipaggiamentoTesta() != null) {
			if (character.getEquipaggiamentoTesta().equals(cosmetic.getNomeCosmetico())) {
				button.setEnabled(false);
			}
		}
		if (character.getEquipaggiamentoCorpo() != null) {
			if (character.getEquipaggiamentoCorpo().equals(cosmetic.getNomeCosmetico())) {
				button.setEnabled(false);
			}
		}
		if (character.getEquipaggiamentoGambe() != null) {
			if (character.getEquipaggiamentoGambe().equals(cosmetic.getNomeCosmetico())) {
				button.setEnabled(false);
			}
		}

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Cosmetics.equipCosmetic(qr, character, cosmetic);
					character = Characters.getCharacter(qr, character.getUsernameUtente(),
							character.getNomePersonaggio());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				new ViewCharacter(character, qr);
				frame.dispose();
			}
		});
		buttons.add(button);

		panel.add(BorderLayout.WEST, infos);
		panel.add(BorderLayout.EAST, buttons);
		return panel;
	}

	private JComponent createShopButton(Cosmetic cosmetic, JFrame frame) {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 10, 5, 10));
		panel.setLayout(new BorderLayout());
		panel.setMaximumSize(new Dimension((this.screenWidth / 3) * 2 - 30, this.screenHeight / 8));

		JPanel infos = new JPanel();
		infos.setLayout(new GridLayout(4, 1, 0, 5));

		JLabel name = new JLabel(cosmetic.getNomeCosmetico());
		name.setFont(new Font("Helvetica", Font.BOLD, 18));
		infos.add(name);
		JLabel gems = new JLabel("Descrizione: " + cosmetic.getDescrizione());
		gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
		infos.add(gems);
		JLabel xp = new JLabel("Tipo: " + cosmetic.getTipo());
		xp.setFont(new Font("Helvetica", Font.PLAIN, 18));
		infos.add(xp);
		JLabel costo = new JLabel("Costo in Gemme: " + cosmetic.getPrezzo());
		costo.setFont(new Font("Helvetica", Font.PLAIN, 18));
		infos.add(costo);

		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(1, 1, 0, 0));

		JButton button = new JButton("Compra Cosmetico");
		if (cosmetic.getPrezzo() > character.getGemme()) {
			button.setEnabled(false);
		}
		button.setFont(new Font("Helvetica", Font.BOLD, 18));
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Cosmetics.purchaseCosmetic(qr, cosmetic, character);
					new ViewCharacter(
							Characters.getCharacter(qr, character.getUsernameUtente(), character.getNomePersonaggio()),
							qr);
					// Characters.alterGems(qr, character, 0 - cosmetic.getPrezzo());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				frame.dispose();
			}
		});
		buttons.add(button);

		panel.add(BorderLayout.WEST, infos);
		panel.add(BorderLayout.EAST, buttons);
		return panel;
	}
}
