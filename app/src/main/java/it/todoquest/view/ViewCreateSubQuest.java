package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.dbutils.QueryRunner;

import com.toedter.calendar.JDateChooser;

import it.todoquest.db.SubQuests;
import it.todoquest.model.Quest;
import it.todoquest.model.SubQuest;

public class ViewCreateSubQuest {
    final QueryRunner qr;
    final Quest mainQuest;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width / 3;
    final int screenHeight = screenSize.height / 4;

    public ViewCreateSubQuest(final QueryRunner qr, final Quest mainQuest) {
        this.qr = qr;
        this.mainQuest = mainQuest;
        try {
            this.initView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initView() throws SQLException {
        // Creating the Frame
        JFrame frame = new JFrame("Crea SubQuest");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(screenWidth, screenHeight);

        // Character Section - Displays Name and Image
        // Creating the panel for the first column
        JPanel questPanel = new JPanel();
        questPanel.setLayout(new GridLayout(4, 2, 10, 10));
        questPanel.setPreferredSize(new Dimension((screenWidth) - 50, screenHeight - 50));

        // Creating the title for the first column
        final JLabel characterLabel = new JLabel("Inserisci Nome Quest: ", JLabel.LEFT);
        characterLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(characterLabel);

        JTextField nomeField = new JTextField(80);
        nomeField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(nomeField);

        final JLabel aspettoLabel = new JLabel("Inserisci una descrizione: ", JLabel.LEFT);
        aspettoLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(aspettoLabel);

        JTextField descrizioneField = new JTextField(80);
        descrizioneField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(descrizioneField);

        final JLabel dataLabel = new JLabel("Scegli una data obiettivo: ", JLabel.LEFT);
        dataLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(dataLabel);

        final JDateChooser calendar = new JDateChooser();
        questPanel.add(calendar);

        final JButton createQuest = new JButton("Conferma");
        createQuest.setFont(new Font("Helvetica", Font.BOLD, 24));
        createQuest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SubQuest quest = new SubQuest();
                quest.setNome(nomeField.getText());
                quest.setDescrizione(descrizioneField.getText());
                if (calendar.getDate()==null) {
                    quest.setDataObiettivo(null);
                }else {
                    quest.setDataObiettivo((new java.sql.Date(calendar.getDate().getTime())));
                }
                quest.setIDMainQuest(mainQuest.getIDMainQuest());

                try {
                    if (SubQuests.getSubQuests(qr, mainQuest).stream()
                            .anyMatch(s -> (s.getNome().equalsIgnoreCase(quest.getNome())))) {
                        JOptionPane.showMessageDialog(null, "La subquest inserita esiste già per questa quest",
                                "Errore", JOptionPane.WARNING_MESSAGE);
                    } else {
                        SubQuests.createSubQuest(qr, quest);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainQuest(mainQuest, qr);
                frame.dispose();
            }
        });
        questPanel.add(createQuest);

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.CENTER, questPanel);
        frame.setVisible(true);
        frame.setResizable(false);
        
        /* Some piece of code */
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                new ViewMainQuest(mainQuest, qr);
            }
        });
    }
}
