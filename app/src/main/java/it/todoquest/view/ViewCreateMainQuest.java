package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.dbutils.QueryRunner;

import com.toedter.calendar.JDateChooser;

import it.todoquest.db.Characters;
import it.todoquest.db.Difficulties;
import it.todoquest.db.Quests;
import it.todoquest.model.Difficulty;
import it.todoquest.model.Quest;

public class ViewCreateMainQuest {
    final QueryRunner qr;
    final String usernameUtente;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width / 3;
    final int screenHeight = screenSize.height / 2;

    public ViewCreateMainQuest(final QueryRunner qr, final String usernameUtente) {
        this.qr = qr;
        this.usernameUtente = usernameUtente;
        try {
            this.initView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initView() throws SQLException {
        // Creating the Frame
        JFrame frame = new JFrame("Crea MainQuest");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(screenWidth, screenHeight);

        // Character Section - Displays Name and Image
        // Creating the panel for the first column
        JPanel questPanel = new JPanel();
        questPanel.setLayout(new GridLayout(6, 2, 10, 10));
        questPanel.setPreferredSize(new Dimension((screenWidth) - 50, screenHeight - 50));

        // Creating the title for the first column
        final JLabel characterLabel = new JLabel("Inserisci Nome Quest: ", JLabel.LEFT);
        characterLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(characterLabel);

        JTextField nomeField = new JTextField(80);
        nomeField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(nomeField);

        final JLabel aspettoLabel = new JLabel("Inserisci una descrizione: ", JLabel.LEFT);
        aspettoLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(aspettoLabel);

        JTextField descrizioneField = new JTextField(80);
        descrizioneField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(descrizioneField);

        final JLabel difficoltàLabel = new JLabel("Scegli una difficoltà: ", JLabel.LEFT);
        difficoltàLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(difficoltàLabel);

        List<Difficulty> listDifficoltà = Difficulties.getDifficulties(qr);
        var difficoltà = listDifficoltà.stream().map(e -> e.getLivelloDifficoltà()).collect(Collectors.toList());
        @SuppressWarnings({ "rawtypes", "unchecked" })
        JComboBox difficoltàClist = new JComboBox(difficoltà.toArray());
        difficoltàClist.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(difficoltàClist);

        final JLabel personaggioLabel = new JLabel("Scegli un personaggio a cui assegnare la quest: ", JLabel.LEFT);
        personaggioLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(personaggioLabel);

        List<it.todoquest.model.Character> listPersonaggi = Characters.getCharacters(qr, usernameUtente);
        var personaggio = listPersonaggi.stream().map(e -> e.getNomePersonaggio()).collect(Collectors.toList());
        @SuppressWarnings({ "rawtypes", "unchecked" })
        JComboBox personaggiClist = new JComboBox(personaggio.toArray());
        personaggiClist.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(personaggiClist);

        final JLabel dataLabel = new JLabel("Scegli una data obiettivo: ", JLabel.LEFT);
        dataLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(dataLabel);

        final JDateChooser calendar = new JDateChooser();
        questPanel.add(calendar);

        final JButton createQuest = new JButton("Conferma");
        createQuest.setFont(new Font("Helvetica", Font.BOLD, 24));
        createQuest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Quest quest = new Quest();
                quest.setNome(nomeField.getText());
                quest.setUsernameUtente(usernameUtente);
                quest.setDescrizione(descrizioneField.getText());
                if (calendar.getDate()==null) {
                    quest.setDataObiettivo(null);
                }else {
                    quest.setDataObiettivo((new java.sql.Date(calendar.getDate().getTime())));
                }
                quest.setLivelloDifficoltà((int) difficoltàClist.getSelectedItem());
                quest.setNomePersonaggio((String) personaggiClist.getSelectedItem());
                try {
                    Quests.createQuest(qr, quest);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainMenu(qr, usernameUtente);
                frame.dispose();
            }
        });
        questPanel.add(createQuest);

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.CENTER, questPanel);
        frame.setVisible(true);
        frame.setResizable(false);

        /* Some piece of code */
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                new ViewMainMenu(qr, usernameUtente);
            }
        });
    }
}
