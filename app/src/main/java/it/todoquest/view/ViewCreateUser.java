package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.dbutils.QueryRunner;

import it.todoquest.db.Users;
import it.todoquest.model.User;

public class ViewCreateUser {
    final QueryRunner qr;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width / 3;
    final int screenHeight = screenSize.height / 3;

    public ViewCreateUser(final QueryRunner qr) {
        this.qr = qr;
        try {
            this.initView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initView() throws SQLException {
        // Creating the Frame
        JFrame frame = new JFrame("Crea Utente");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(screenWidth, screenHeight);

        // Character Section - Displays Name and Image
        // Creating the panel for the first column
        JPanel questPanel = new JPanel();
        questPanel.setLayout(new GridLayout(4, 2, 10, 10));
        questPanel.setPreferredSize(new Dimension((screenWidth) - 50, screenHeight - 50));

        // Creating the title for the first column
        final JLabel characterLabel = new JLabel("Inserisci Nome Utente: ", JLabel.LEFT);
        characterLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(characterLabel);

        JTextField nomeField = new JTextField(80);
        nomeField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(nomeField);

        final JLabel aspettoLabel = new JLabel("Inserisci Cognome Utente: ", JLabel.LEFT);
        aspettoLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(aspettoLabel);

        JTextField cognomeField = new JTextField(80);
        cognomeField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(cognomeField);

        final JLabel dataLabel = new JLabel("Scegli Username Utente univoco: ", JLabel.LEFT);
        dataLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(dataLabel);

        JTextField usernameField = new JTextField(80);
        usernameField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        questPanel.add(usernameField);

        final JButton createQuest = new JButton("Conferma");
        createQuest.setFont(new Font("Helvetica", Font.BOLD, 24));
        createQuest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                User user = new User();
                user.setNome(nomeField.getText());
                user.setCognome(cognomeField.getText());
                user.setUsernameUtente(usernameField.getText());

                try {
                    if (Users.getUsers(qr).stream().map(u -> u.getUsernameUtente())
                            .anyMatch(u -> u.equalsIgnoreCase(user.getUsernameUtente()))) {
                        JOptionPane.showMessageDialog(null, "L'username indicato esiste già!", "Errore",
                                JOptionPane.WARNING_MESSAGE);
                    } else {
                        Users.createUser(qr, user);
                        new ViewMainMenu(qr, user.getUsernameUtente());
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
        questPanel.add(createQuest);

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.CENTER, questPanel);
        frame.setVisible(true);
        frame.setResizable(false);

        /* Some piece of code */
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                System.exit(1);
            }
        });
    }
}
