package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.dbutils.QueryRunner;

import it.todoquest.db.Categories;
import it.todoquest.db.Characters;
import it.todoquest.db.Quests;
import it.todoquest.db.SubQuests;
import it.todoquest.model.Category;
import it.todoquest.model.Quest;
import it.todoquest.model.SubQuest;

public class ViewMainQuest {
    final Quest quest;
    final QueryRunner qr;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width - (screenSize.width / 7);
    final int screenHeight = screenSize.height - (screenSize.height / 7);

    public ViewMainQuest(final Quest quest, final QueryRunner qr) {
        this.quest = quest;
        this.qr = qr;
        try {
            this.initView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initView() throws SQLException {
        // Creating the Frame
        JFrame frame = new JFrame(this.quest.getNome());
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(screenWidth, screenHeight);

        // Quest Section - Displays Name and Description
        // Creating the panel for the first column
        JPanel mainquestPanel = new JPanel();
        mainquestPanel.setLayout(new BoxLayout(mainquestPanel, BoxLayout.Y_AXIS));
        mainquestPanel.setPreferredSize(new Dimension((screenWidth / 2) - 20, screenHeight - 50));

        // Creating the title for the first column
        final JLabel mainquestLabel = new JLabel(this.quest.getNome(), JLabel.LEFT);
        mainquestLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        mainquestPanel.add(mainquestLabel);

        final JLabel descriptionLabel = new JLabel("<html><br/><b>Descrizione: </b><br/>" + this.quest.getDescrizione() + "</html>", JLabel.LEFT);
        descriptionLabel.setFont(new Font("Helvetica", Font.PLAIN, 36));
        mainquestPanel.add(descriptionLabel);
        
        final JLabel characterFather = new JLabel("<html><br/><b>Personaggio: </b>" + this.quest.getNomePersonaggio() + "</html>", JLabel.LEFT);
        characterFather.setFont(new Font("Helvetica", Font.PLAIN, 36));
        mainquestPanel.add(characterFather);

        final JLabel dateCreationLabel = new JLabel("Data Creazione: " + this.quest.getDataCreazione(), JLabel.LEFT);
        dateCreationLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        mainquestPanel.add(dateCreationLabel);

        String dataObiettivo;
        if (this.quest.getDataObiettivo()==null) {
            dataObiettivo = "Niente";
        }else {
            dataObiettivo = this.quest.getDataObiettivo().toString();
        }
        final JLabel dateTargetLabel = new JLabel("Data Obiettivo: " + dataObiettivo, JLabel.LEFT);
        dateTargetLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        mainquestPanel.add(dateTargetLabel);

        final JLabel gemsLabel = new JLabel("Gemme ricompensa: " + this.quest.getRicompensaGemme(), JLabel.LEFT);
        gemsLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        mainquestPanel.add(gemsLabel);

        final JLabel XPLabel = new JLabel("XP ricompensa: " + this.quest.getRicompensaXP(), JLabel.LEFT);
        XPLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        mainquestPanel.add(XPLabel);

        final JButton completeQuestButton = new JButton("Completa Quest");
        completeQuestButton.setFont(new Font("Helvetica", Font.BOLD, 36));
        completeQuestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    it.todoquest.model.Character character = new it.todoquest.model.Character();
                    character.setNomePersonaggio(quest.getNomePersonaggio());
                    character.setUsernameUtente(quest.getUsernameUtente());

                    Characters.alterGems(qr, character, quest.getRicompensaGemme());
                    Characters.alterXP(qr, character, quest.getRicompensaXP());
                    Quests.completeMainQuest(qr, quest);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainMenu(qr, quest.getUsernameUtente());
                frame.dispose();
            }
        });
        mainquestPanel.add(completeQuestButton);

        final JLabel categorieLabel = new JLabel("Scegli una categoria: ", JLabel.LEFT);
        categorieLabel.setFont(new Font("Helvetica", Font.PLAIN, 24));
        mainquestPanel.add(categorieLabel);

        List<Category> categorie = Categories.getCategories(qr, quest.getUsernameUtente());
        @SuppressWarnings({ "rawtypes", "unchecked" })
        JList<Category> categorieCList = new JList(categorie.toArray());
        categorieCList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        categorieCList.setFont(new Font("Helvetica", Font.PLAIN, 24));
        List<Category> categorieQuest = Categories.getQuestCategories(qr, quest);
        categorieCList.setSelectedIndices(
                IntStream.range(0, categorie.size()).filter(i -> categorieQuest.contains(categorie.get(i))).toArray());
        JScrollPane categoriaScroll = new JScrollPane(categorieCList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        mainquestPanel.add(categoriaScroll);

        final JButton assignCategoryButton = new JButton("Assegna Categoria");
        assignCategoryButton.setFont(new Font("Helvetica", Font.BOLD, 36));
        assignCategoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Categories.resetCategories(qr, quest);
                    for (Category categoria : categorieCList.getSelectedValuesList()) {
                        Quests.assignCategory(qr, quest, categoria);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainQuest(quest, qr);
                frame.dispose();
            }
        });
        mainquestPanel.add(assignCategoryButton);

        final JButton createCategoria = new JButton("Crea Categoria");
        createCategoria.setFont(new Font("Helvetica", Font.BOLD, 24));
        createCategoria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s = (String) JOptionPane.showInputDialog(frame, "Inserisci nome categoria", "Customized Dialog",
                        JOptionPane.PLAIN_MESSAGE);
                if (s != null) {
                    Category categoria = new Category();
                    categoria.setNomeCategoria(s);
                    categoria.setUsernameUtente(quest.getUsernameUtente());

                    try {
                        if (Categories.getCategories(qr, quest.getUsernameUtente()).contains(categoria)) {
                            JOptionPane.showMessageDialog(null,
                                    "Una categoria con questo nome esiste già per questo utente!", "Errore",
                                    JOptionPane.WARNING_MESSAGE);
                        }else {
                            Categories.createCategory(qr, categoria);
                        }
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                new ViewMainQuest(quest, qr);
                frame.dispose();

            }
        });
        mainquestPanel.add(createCategoria);

        // Subquest Section
        // Creating the panel for the second column
        JPanel subquestPanel = new JPanel();
        subquestPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
        subquestPanel.setPreferredSize(new Dimension((screenWidth / 2) - 20, screenHeight - 50));

        final JLabel subquestLabel = new JLabel("SubQuests", JLabel.CENTER);
        subquestLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        subquestPanel.add(subquestLabel);

        final JButton createSubQuest = new JButton("Crea SubQuest");
        createSubQuest.setFont(new Font("Helvetica", Font.PLAIN, 28));
        createSubQuest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ViewCreateSubQuest(qr, quest);
                frame.dispose();
            }
        });
        subquestPanel.add(createSubQuest);

        List<SubQuest> subquestList = null;
        try {
            subquestList = SubQuests.getSubQuests(qr, quest);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        JPanel subquestButtonsPanel = new JPanel();
        subquestButtonsPanel.setLayout(new BoxLayout(subquestButtonsPanel, BoxLayout.Y_AXIS));
        subquestList.stream().map(s -> this.createSubQuestButton(s, quest, frame)).forEach(subquestButtonsPanel::add);

        // Creating the ScrollPane for the buttons
        JScrollPane subquestButtonsScroll = new JScrollPane(subquestButtonsPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        subquestButtonsScroll.setPreferredSize(new Dimension((screenWidth / 2) - 50, (screenHeight) - 150));
        subquestPanel.add(subquestButtonsScroll);

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.WEST, mainquestPanel);
        frame.getContentPane().add(BorderLayout.EAST, subquestPanel);
        frame.setVisible(true);
        frame.setResizable(false);

        /* Some piece of code */
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                new ViewMainMenu(qr, quest.getUsernameUtente());
            }
        });
    }

    private JComponent createSubQuestButton(SubQuest quest, Quest mainquest, JFrame frame) {
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 10, 5, 10));
        panel.setLayout(new BorderLayout());
        panel.setMaximumSize(new Dimension(this.screenWidth / 2 - 20, this.screenHeight / 10));

        JPanel infos = new JPanel();
        infos.setLayout(new GridLayout(3, 1, 0, 5));

        JLabel name = new JLabel(quest.getNome());
        name.setFont(new Font("Helvetica", Font.BOLD, 18));
        infos.add(name);
        JLabel gems = new JLabel("Descrizione: " + quest.getDescrizione());
        gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(gems);
        String dataObiettivo;
        if (quest.getDataObiettivo()==null) {
            dataObiettivo = "Niente";
        }else {
            dataObiettivo = quest.getDataObiettivo().toString();
        }
        JLabel data = new JLabel("Data Obiettivo: " + dataObiettivo);
        data.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(data);

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 1, 0, 0));

        JButton button = new JButton("Completa SubQuest");
        button.setFont(new Font("Helvetica", Font.BOLD, 18));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    SubQuests.completeSubQuest(qr, quest);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainQuest(mainquest, qr);
                frame.dispose();
            }
        });
        buttons.add(button);

        panel.add(BorderLayout.WEST, infos);
        panel.add(BorderLayout.EAST, buttons);
        return panel;
    }

}
