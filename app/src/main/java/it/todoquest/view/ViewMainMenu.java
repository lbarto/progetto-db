package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.apache.commons.dbutils.QueryRunner;

import it.todoquest.db.ChallengeRatings;
import it.todoquest.db.Characters;
import it.todoquest.db.Dungeons;
import it.todoquest.db.Enemies;
import it.todoquest.db.Quests;
import it.todoquest.model.ChallengeRating;
import it.todoquest.model.Dungeon;
import it.todoquest.model.DungeonSetting;
import it.todoquest.model.Enemy;
import it.todoquest.model.Quest;

public class ViewMainMenu {
    final QueryRunner qr;
    final String username;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width - (screenSize.width / 10);
    final int screenHeight = screenSize.height - (screenSize.height / 10);
    final int panelWidth = screenWidth / 3;
    final int resizeWidth = panelWidth / 10;
    final int resizeHeight = screenHeight / 7;
    final int resizeEntry = panelWidth / 10;

    public ViewMainMenu(QueryRunner qr, String username) {
        this.qr = qr;
        this.username = username;
        try {
            this.initView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initView() throws SQLException {
        // Creating the Frame
        JFrame frame = new JFrame("To-Do Quest");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(this.screenWidth, this.screenHeight);

        // --------------------------------------------------

        // Character Section
        // Creating the panel for the first column
        JPanel characterPanel = new JPanel();
        characterPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
        characterPanel.setPreferredSize(new Dimension(this.panelWidth - this.resizeWidth, this.screenHeight - 50));

        // Creating the title for the first column
        final JLabel characterLabel = new JLabel("Personaggi", JLabel.CENTER);
        characterLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        characterPanel.add(characterLabel);

        final JButton createCharacter = new JButton("Crea Personaggio");
        createCharacter.setFont(new Font("Helvetica", Font.PLAIN, 28));
        createCharacter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ViewCreateCharacter(qr, username);
                frame.dispose();
            }
        });
        characterPanel.add(createCharacter);

        // Creating the list of buttons for the first column
        List<it.todoquest.model.Character> characterList = null;
        try {
            characterList = Characters.getCharacters(this.qr, this.username);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        JPanel characterButtonsPanel = new JPanel();
        characterButtonsPanel.setLayout(new BoxLayout(characterButtonsPanel, BoxLayout.Y_AXIS));
        characterList.stream().map(c -> this.createCharacterButton(c, frame)).forEach(characterButtonsPanel::add);

        // Creating the ScrollPane for the buttons
        JScrollPane characterButtonsScroll = new JScrollPane(characterButtonsPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        characterButtonsScroll.setPreferredSize(
                new Dimension(this.panelWidth - this.resizeWidth, this.screenHeight - this.resizeHeight));
        characterPanel.add(characterButtonsScroll);

        // --------------------------------------------------

        // Quest Section
        // Creating the panel for the second column
        JPanel questPanel = new JPanel();
        questPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
        questPanel.setPreferredSize(new Dimension(this.panelWidth - this.resizeWidth, this.screenHeight - 50));

        // Creating the title for the second column
        final JLabel questLabel = new JLabel("   MainQuests   ", JLabel.CENTER);
        questLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        questPanel.add(questLabel);

        final JButton createQuest = new JButton("Crea MainQuest");
        createQuest.setFont(new Font("Helvetica", Font.PLAIN, 28));
        if (Characters.getCharacters(qr, username).size()==0) {
            createQuest.setEnabled(false);
        }
        createQuest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ViewCreateMainQuest(qr, username);
                frame.dispose();
            }
        });
        questPanel.add(createQuest);

        // Creating the list of buttons for the second column;
        List<Quest> questList = null;
        try {
            questList = Quests.getQuests(this.qr, this.username);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        JPanel questButtonsPanel = new JPanel();
        questButtonsPanel.setLayout(new BoxLayout(questButtonsPanel, BoxLayout.Y_AXIS));
        questList.stream().map(q -> this.createQuestButton(q, frame)).forEach(questButtonsPanel::add);

        // Creating the ScrollPane for the buttons
        JScrollPane questButtonsScroll = new JScrollPane(questButtonsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        questButtonsScroll.setPreferredSize(
                new Dimension(this.panelWidth - this.resizeWidth, this.screenHeight - this.resizeHeight));
        questPanel.add(questButtonsScroll);

        // --------------------------------------------------

        // Dungeon Section
        // Creating the panel for the second column
        JPanel dungeonPanel = new JPanel();
        dungeonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
        dungeonPanel.setPreferredSize(new Dimension(this.panelWidth - this.resizeWidth, this.screenHeight - 50));
        // Creating the title for the second column
        final JLabel dungeonLabel = new JLabel("Dungeons", JLabel.CENTER);
        dungeonLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        dungeonPanel.add(dungeonLabel);

        final JButton createDungeon = new JButton("Genera Dungeon");
        createDungeon.setFont(new Font("Helvetica", Font.PLAIN, 28));
        if (Characters.getCharacters(qr, username).size()==0) {
            createDungeon.setEnabled(false);
        }
        createDungeon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Random r = new Random();
                Dungeon dungeon = new Dungeon();
                String[] nomeList = { "del Caos", "della Solitudine", "dei Cappucci Rossi", "della Morte", "di Fuoco" };
                try {
                    dungeon.setUsernameUtente(username);

                    List<DungeonSetting> ambientazioneList = Dungeons.getDungeonSettings(qr);
                    var ambientazioni = ambientazioneList.stream().map(g -> g.getNomeAmbientazione())
                            .collect(Collectors.toList());
                    dungeon.setNomeAmbientazione(ambientazioni.get(r.nextInt(ambientazioni.size())));

                    dungeon.setNomeDungeon(dungeon.getNomeAmbientazione() + " " + nomeList[r.nextInt(nomeList.length)]);
                    
                    double diff = 999;
                    int idGS = 0;
                    for (ChallengeRating gradoSfida : ChallengeRatings.getChallengeRatings(qr)) {
                        if (Math.abs(gradoSfida.getIDGradoSfida() - Dungeons.getPartyLevel(qr, username)) < diff) {
                            diff = Math.abs(gradoSfida.getIDGradoSfida() - Dungeons.getPartyLevel(qr, username));
                            idGS = gradoSfida.getIDGradoSfida();
                        }
                    }
                    dungeon.setIDGradoSfida(idGS);

                    int maxID = Dungeons.getDungeons(qr, username).stream().mapToInt(j -> j.getIDDungeon()).max()
                            .orElse(0);
                    dungeon.setIDDungeon(maxID + 1);

                    Dungeons.createDungeon(qr, dungeon);

                    List<Enemy> nemici = Enemies.getAllEnemies(qr);
                    List<Enemy> nemiciValidi = new LinkedList<>();
                    Enemy nemico = null;
                    int gradoSfidaNemici = 0;
                    
                    while (gradoSfidaNemici < dungeon.getIDGradoSfida()) {
                        for (Enemy nem : nemici) {
                            if (nem.getLivello() <= (dungeon.getIDGradoSfida() - gradoSfidaNemici)) {
                                nemiciValidi.add(nem);
                            }
                        }
                        if (nemiciValidi.size() == 0) {
                            break;
                        } else {
                            nemico = nemiciValidi.get(r.nextInt(nemiciValidi.size()));
                            Dungeons.populateDungeon(qr, dungeon, nemico);
                            gradoSfidaNemici = gradoSfidaNemici + nemico.getLivello();
                            nemiciValidi = new LinkedList<>();
                        }
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                new ViewMainMenu(qr, username);
                frame.dispose();
            }
        });
        dungeonPanel.add(createDungeon);

        // Creating the list of buttons for the third column;
        List<Dungeon> dungeonList = null;
        try {
            dungeonList = Dungeons.getDungeons(this.qr, this.username);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        JPanel dungeonButtonsPanel = new JPanel();
        dungeonButtonsPanel.setLayout(new BoxLayout(dungeonButtonsPanel, BoxLayout.Y_AXIS));
        dungeonList.stream().map(d -> this.createDungeonButton(d, frame)).forEach(dungeonButtonsPanel::add);

        // Creating the ScrollPane for the buttons
        JScrollPane dungeonButtonsScroll = new JScrollPane(dungeonButtonsPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        dungeonButtonsScroll.setPreferredSize(
                new Dimension(this.panelWidth - this.resizeWidth, this.screenHeight - this.resizeHeight));
        dungeonPanel.add(dungeonButtonsScroll);

        // Adding Components to the frame.
        frame.setLayout(new GridLayout(1, 3, 20, 0));
        frame.add(characterPanel);
        frame.add(questPanel);
        frame.add(dungeonPanel);

        frame.setResizable(false);
        frame.setVisible(true);
    }

    private JComponent createCharacterButton(it.todoquest.model.Character character, JFrame frame) {
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 10, 5, 10));
        panel.setLayout(new BorderLayout());
        panel.setMaximumSize(new Dimension(this.panelWidth - this.resizeEntry, this.screenHeight / 10));

        JPanel infos = new JPanel();
        infos.setLayout(new GridLayout(3, 1, 0, 5));

        JLabel name = new JLabel(character.getNomePersonaggio());
        name.setFont(new Font("Helvetica", Font.BOLD, 18));
        infos.add(name);
        JLabel gems = new JLabel("Gemme: " + character.getGemme());
        gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(gems);
        JLabel xp = new JLabel("XP: " + character.getPuntiXP());
        xp.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(xp);

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 1, 0, 0));

        JButton button = new JButton("Visualizza Personaggio");
        button.setFont(new Font("Helvetica", Font.BOLD, 18));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ViewCharacter(character, qr);
                frame.dispose();
            }
        });
        buttons.add(button);

        panel.add(BorderLayout.WEST, infos);
        panel.add(BorderLayout.EAST, buttons);
        return panel;
    }

    private JComponent createQuestButton(Quest quest, JFrame frame) {
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 10, 5, 10));
        panel.setLayout(new BorderLayout());
        panel.setMaximumSize(new Dimension(this.panelWidth - this.resizeEntry, this.screenHeight / 10));

        JPanel infos = new JPanel();
        infos.setLayout(new GridLayout(3, 1, 0, 5));

        JLabel name = new JLabel(quest.getNome());
        name.setFont(new Font("Helvetica", Font.BOLD, 18));
        infos.add(name);
        JLabel gems = new JLabel("Gemme: " + quest.getRicompensaGemme());
        gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(gems);
        JLabel xp = new JLabel("XP: " + quest.getRicompensaXP());
        xp.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(xp);

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 1, 0, 0));

        JButton button = new JButton("Visualizza MainQuest");
        button.setFont(new Font("Helvetica", Font.BOLD, 18));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ViewMainQuest(quest, qr);
                frame.dispose();
            }
        });
        buttons.add(button);

        panel.add(BorderLayout.WEST, infos);
        panel.add(BorderLayout.EAST, buttons);
        return panel;
    }

    private JComponent createDungeonButton(Dungeon dungeon, JFrame frame) {
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 10, 5, 10));
        panel.setLayout(new BorderLayout());
        panel.setMaximumSize(new Dimension(this.panelWidth - this.resizeEntry, this.screenHeight / 10));

        JPanel infos = new JPanel();
        infos.setLayout(new GridLayout(3, 1, 0, 5));

        JLabel name = new JLabel(dungeon.getNomeDungeon());
        name.setFont(new Font("Helvetica", Font.BOLD, 18));
        infos.add(name);
        JLabel gems = new JLabel("Grado Sfida: " + dungeon.getIDGradoSfida());
        gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(gems);
        JLabel xp = new JLabel("Ambientazione: " + dungeon.getNomeAmbientazione());
        xp.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(xp);

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 1, 0, 0));

        JButton button = new JButton("Visualizza Dungeon");
        button.setFont(new Font("Helvetica", Font.BOLD, 18));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ViewDungeon(dungeon, qr, username);
                frame.dispose();
            }
        });
        buttons.add(button);

        panel.add(BorderLayout.WEST, infos);
        panel.add(BorderLayout.EAST, buttons);
        return panel;
    }
}
