package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.dbutils.QueryRunner;

import it.todoquest.db.Characters;
import it.todoquest.db.Dungeons;
import it.todoquest.db.Enemies;
import it.todoquest.model.Dungeon;
import it.todoquest.model.DungeonDetails;
import it.todoquest.model.Enemy;

public class ViewDungeon {
    final Dungeon dungeon;
    final QueryRunner qr;
    DungeonDetails dungeonDetails;
    final String usernameUtente;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width - (screenSize.width / 7);
    final int screenHeight = screenSize.height - (screenSize.height / 7);

    public ViewDungeon(final Dungeon dungeon, final QueryRunner qr, final String usernameUtente) {
        this.dungeonDetails = null;
        this.dungeon = dungeon;
        this.qr = qr;
        this.usernameUtente = usernameUtente;
        try {
            this.dungeonDetails = Dungeons.getDungeonDetails(qr, this.dungeon).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.initView();
    }

    public void initView() {
        // Creating the Frame
        JFrame frame = new JFrame(this.dungeon.getNomeDungeon());
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(screenWidth, screenHeight);

        // Quest Section - Displays Name and Description
        // Creating the panel for the first column
        JPanel dungeonPanel = new JPanel();
        dungeonPanel.setLayout(new BoxLayout(dungeonPanel, BoxLayout.Y_AXIS));
        dungeonPanel.setPreferredSize(new Dimension((screenWidth / 2) - 20, screenHeight - 50));

        // Creating the title for the first column
        final JLabel dungeonLabel = new JLabel(this.dungeon.getNomeDungeon(), JLabel.LEFT);
        dungeonLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        dungeonPanel.add(dungeonLabel);

        final JLabel descriptionLabel = new JLabel("<html><br/>GradoSfida: " + this.dungeon.getIDGradoSfida() + "</html>", JLabel.LEFT);
        descriptionLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        dungeonPanel.add(descriptionLabel);

        final JLabel dateCreationLabel = new JLabel("Ambientazione: " + this.dungeon.getNomeAmbientazione(),
                JLabel.LEFT);
        dateCreationLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        dungeonPanel.add(dateCreationLabel);

        final JLabel dateTargetLabel = new JLabel("<html><b>Descrizione Ambientazione: </b><br/>" + this.dungeonDetails.getDescrizione() + "</html>",
                JLabel.LEFT);
        dateTargetLabel.setFont(new Font("Helvetica", Font.PLAIN, 36));
        dungeonPanel.add(dateTargetLabel);

        final JLabel gemsLabel = new JLabel("Gemme ricompensa: " + this.dungeonDetails.getRicompensaGemme(),
                JLabel.LEFT);
        gemsLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        dungeonPanel.add(gemsLabel);

        final JLabel XPLabel = new JLabel("Gemme penalità: " + this.dungeonDetails.getPenalitàGemme(), JLabel.LEFT);
        XPLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        dungeonPanel.add(XPLabel);

        final JButton enterDungeon = new JButton("Affronta Dungeon");
        enterDungeon.setFont(new Font("Helvetica", Font.BOLD, 36));
        enterDungeon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double partyLevel = Dungeons.getPartyLevelDungeon(qr, dungeon);
                    double dungeonLevel = (double) dungeon.getIDGradoSfida();
                    double probability = ((partyLevel / dungeonLevel) * 0.5);
                    Random r = new Random();

                    if (r.nextDouble() < probability) {
                        JOptionPane.showMessageDialog(frame, "Il tuo party ha vinto il dungeon!");
                        for (it.todoquest.model.Character character : Characters.getCharacters(qr,
                                dungeon.getUsernameUtente())) {
                            Characters.alterGems(qr, character, dungeonDetails.getRicompensaGemme());
                        }
                        Dungeons.deleteDungeon(qr, dungeon);

                    } else {
                        JOptionPane.showMessageDialog(frame, "Il tuo party ha perso il dungeon!");
                        for (it.todoquest.model.Character character : Characters.getCharacters(qr,
                                dungeon.getUsernameUtente())) {
                            Characters.alterGems(qr, character, 0 - dungeonDetails.getPenalitàGemme());
                        }
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainMenu(qr, usernameUtente);
                frame.dispose();
            }
        });
        dungeonPanel.add(enterDungeon);

        // Monster Section
        // Creating the panel for the second column
        JPanel enemyPanel = new JPanel();
        enemyPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
        enemyPanel.setPreferredSize(new Dimension((screenWidth / 2) - 20, screenHeight - 50));

        final JLabel enemyLabel = new JLabel("Nemici", JLabel.CENTER);
        enemyLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
        enemyPanel.add(enemyLabel);

        List<Enemy> enemyList = null;
        try {
            enemyList = Enemies.getDungeonEnemies(qr, dungeon);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        JPanel enemyButtonsPanel = new JPanel();
        enemyButtonsPanel.setLayout(new BoxLayout(enemyButtonsPanel, BoxLayout.Y_AXIS));
        enemyList.stream().map(this::createEnemyButton).forEach(enemyButtonsPanel::add);

        // Creating the ScrollPane for the buttons
        JScrollPane enemyButtonsScroll = new JScrollPane(enemyButtonsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        enemyButtonsScroll.setPreferredSize(new Dimension((screenWidth / 2) - 50, (screenHeight) - 150));
        enemyPanel.add(enemyButtonsScroll);

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.WEST, dungeonPanel);
        frame.getContentPane().add(BorderLayout.EAST, enemyPanel);
        frame.setVisible(true);
        frame.setResizable(false);

        /* Some piece of code */
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                new ViewMainMenu(qr, usernameUtente);
            }
        });
    }

    private JComponent createEnemyButton(Enemy enemy) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setMaximumSize(new Dimension(this.screenWidth / 2 - 80, this.screenHeight / 10));

        JPanel infos = new JPanel();
        infos.setLayout(new GridLayout(3, 1, 0, 5));

        JLabel name = new JLabel(enemy.getNome());
        name.setFont(new Font("Helvetica", Font.BOLD, 18));
        infos.add(name);
        JLabel gems = new JLabel("Descrizione: " + enemy.getDescrizione());
        gems.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(gems);
        JLabel xp = new JLabel("Livello: " + enemy.getLivello());
        xp.setFont(new Font("Helvetica", Font.PLAIN, 18));
        infos.add(xp);

        panel.add(BorderLayout.CENTER, infos);
        return panel;
    }
}
