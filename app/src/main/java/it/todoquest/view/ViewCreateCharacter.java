package it.todoquest.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.dbutils.QueryRunner;

import it.todoquest.db.Appearances;
import it.todoquest.db.CharacterClasses;
import it.todoquest.db.Characters;
import it.todoquest.model.Appearance;
import it.todoquest.model.CharacterClass;

public class ViewCreateCharacter {
    final QueryRunner qr;
    final String usernameUtente;

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenWidth = screenSize.width / 4;
    final int screenHeight = screenSize.height / 4;

    public ViewCreateCharacter(final QueryRunner qr, final String usernameUtente) {
        this.qr = qr;
        this.usernameUtente = usernameUtente;
        try {
            this.initView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initView() throws SQLException {
        // Creating the Frame
        JFrame frame = new JFrame("Crea Personaggio");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Setting the Sizes according to the screen
        frame.setSize(screenWidth, screenHeight);

        // Character Section - Displays Name and Image
        // Creating the panel for the first column
        JPanel characterPanel = new JPanel();
        characterPanel.setLayout(new GridLayout(4, 2, 10, 10));
        characterPanel.setPreferredSize(new Dimension((screenWidth) - 50, screenHeight - 50));

        // Creating the title for the first column
        final JLabel characterLabel = new JLabel("Inserisci Nome Personaggio: ", JLabel.LEFT);
        characterLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        characterPanel.add(characterLabel);

        JTextField xField = new JTextField(80);
        xField.setFont(new Font("Helvetica", Font.PLAIN, 14));
        characterPanel.add(xField);

        final JLabel aspettoLabel = new JLabel("Scegli un aspetto: ", JLabel.LEFT);
        aspettoLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        characterPanel.add(aspettoLabel);

        List<Appearance> listAsp = Appearances.getAspects(qr);
        @SuppressWarnings({ "rawtypes", "unchecked" })
        JComboBox aspettoCList = new JComboBox(listAsp.toArray());
        aspettoCList.setFont(new Font("Helvetica", Font.PLAIN, 14));
        characterPanel.add(aspettoCList);

        final JLabel classeLabel = new JLabel("Scegli una classe: ", JLabel.LEFT);
        classeLabel.setFont(new Font("Helvetica", Font.PLAIN, 14));
        characterPanel.add(classeLabel);

        List<CharacterClass> listClassi = CharacterClasses.getClasses(qr);
        @SuppressWarnings({ "rawtypes", "unchecked" })
        JComboBox classeCList = new JComboBox(listClassi.toArray());
        classeCList.setFont(new Font("Helvetica", Font.PLAIN, 14));
        characterPanel.add(classeCList);

        final JButton createCharacter = new JButton("Conferma");
        createCharacter.setFont(new Font("Helvetica", Font.BOLD, 24));
        createCharacter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                it.todoquest.model.Character character = new it.todoquest.model.Character();
                character.setNomePersonaggio(xField.getText());
                character.setUsernameUtente(usernameUtente);
                character.setGemme(0);
                character.setPuntiXP(0);
                character.setEquipaggiamentoCorpo(null);
                character.setEquipaggiamentoGambe(null);
                character.setEquipaggiamentoTesta(null);
                character.setNomeAspetto(((Appearance) aspettoCList.getSelectedItem()).getNomeAspetto());
                character.setNomeClasse(((CharacterClass) classeCList.getSelectedItem()).getNomeClasse());
                try {
                    if (Characters.getCharacters(qr, usernameUtente).stream()
                            .anyMatch(c -> c.getNomePersonaggio().equalsIgnoreCase(character.getNomePersonaggio()))) {
                        JOptionPane.showMessageDialog(null,
                                "Un personaggio con questo nome esiste già per questo utente!", "Errore",
                                JOptionPane.WARNING_MESSAGE);
                    } else {
                        Characters.createCharacter(qr, character);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                new ViewMainMenu(qr, usernameUtente);
                frame.dispose();
            }
        });
        characterPanel.add(createCharacter);

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.CENTER, characterPanel);
        frame.setVisible(true);
        frame.setResizable(false);

        /* Some piece of code */
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                new ViewMainMenu(qr, usernameUtente);
            }
        });
    }
}
