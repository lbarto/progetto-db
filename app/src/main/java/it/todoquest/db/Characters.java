package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Character;

public class Characters {
    public static int createCharacter(QueryRunner runner, Character character) throws SQLException {
        return runner.update("insert into personaggi values(?, ?, ?, ?, ?, ?, ?, ?, ?)", character.getUsernameUtente(),
                character.getNomePersonaggio(), character.getPuntiXP(), character.getGemme(),
                character.getNomeAspetto(), character.getNomeClasse(), null, null, null);
    }

    public static List<Character> getCharacters(QueryRunner runner, String username) throws SQLException {
        return runner.query("select * " + "from personaggi p " + "where p.UsernameUtente = ?",
                new BeanListHandler<>(Character.class), username);
    }
    
    public static Character getCharacter(QueryRunner runner, String username, String NomePersonaggio) throws SQLException {
        return runner.query("select * " + "from personaggi p " + "where p.UsernameUtente = ? AND p.NomePersonaggio = ?",
                new BeanHandler<>(Character.class), username, NomePersonaggio);
    }

    public static int alterGems(QueryRunner runner, Character character, int amount) throws SQLException {
        return runner.update(
                "UPDATE personaggi p SET p.Gemme = (CASE WHEN(p.Gemme + ?>-1) THEN p.Gemme + ? ELSE 0 END) WHERE p.UsernameUtente = ? AND p.NomePersonaggio = ?",
                amount, amount, character.getUsernameUtente(), character.getNomePersonaggio());
    }

    public static int alterXP(QueryRunner runner, Character character, int amount) throws SQLException {
        return runner.update(
                "UPDATE personaggi p SET p.PuntiXP = p.PuntiXP + ? WHERE p.UsernameUtente = ? AND p.NomePersonaggio = ?",
                amount, character.getUsernameUtente(), character.getNomePersonaggio());
    }
}