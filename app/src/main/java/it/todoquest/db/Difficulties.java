package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Difficulty;

public class Difficulties {
    public static List<Difficulty> getDifficulties(QueryRunner runner) throws SQLException {
        return runner.query("SELECT * FROM difficoltà", new BeanListHandler<>(Difficulty.class));
    }
}