package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Category;
import it.todoquest.model.Quest;

public class Quests {
    public static List<Quest> getQuests(QueryRunner runner, String username)
            throws SQLException {
        return runner.query("select m.*, d.RicompensaGemme, d.RicompensaXP\r\n" + "from mainquest m, difficoltà d "
                + "where m.LivelloDifficoltà = d.LivelloDifficoltà and m.UsernameUtente = ? and m.Completato = false\r\n",
                new BeanListHandler<>(Quest.class), username);
    }

    public static int createQuest(QueryRunner runner, Quest newQuest) throws SQLException {
        return runner.update(
                "insert into mainquest(UsernameUtente, NomePersonaggio, Nome, DataCreazione, Completato, Descrizione, DataObiettivo, LivelloDifficoltà)"
                        + "values(?, ?, ?, current_timestamp(), false, ?, ?, ?)",
                newQuest.getUsernameUtente(), newQuest.getNomePersonaggio(), newQuest.getNome(),
                newQuest.getDescrizione(), newQuest.getDataObiettivo(), newQuest.getLivelloDifficoltà());
    }

    public static int completeMainQuest(QueryRunner runner, Quest quest) throws SQLException {
        return runner.update("update mainquest\r\n" + "set Completato = true\r\n" + "where IDMainQuest = ?\r\n" + "",
                quest.getIDMainQuest());
    }

    public static int assignCategory(QueryRunner runner, Quest quest, Category cat) throws SQLException {
        return runner.update("insert into appartenenzecategoria values (?,?,?)", cat.getUsernameUtente(),
                cat.getNomeCategoria(), quest.getIDMainQuest());
    }
}