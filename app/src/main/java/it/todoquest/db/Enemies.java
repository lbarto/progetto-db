package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Dungeon;
import it.todoquest.model.Enemy;

public class Enemies {
    public static List<Enemy> getDungeonEnemies(QueryRunner runner, Dungeon dungeon) throws SQLException {
        return runner.query(
                "SELECT\r\n" + "    n.*\r\n" + "FROM\r\n" + "    popolazionedungeon p,\r\n" + "    nemici n\r\n"
                        + "WHERE\r\n" + "    p.UsernameUtente = ?\r\n" + "        AND p.IDDungeon = ?\r\n"
                        + "        AND n.IDNemico = p.IDNemico",
                new BeanListHandler<>(Enemy.class), dungeon.getUsernameUtente(), dungeon.getIDDungeon());
    }

    public static List<Enemy> getAllEnemies(QueryRunner runner) throws SQLException {
        return runner.query("select * from nemici", new BeanListHandler<>(Enemy.class));
    }

}