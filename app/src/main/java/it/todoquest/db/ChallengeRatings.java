package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.ChallengeRating;

public class ChallengeRatings {
    public static List<ChallengeRating> getChallengeRatings(QueryRunner runner) throws SQLException {
        return runner.query("select * from gradisfida", new BeanListHandler<>(ChallengeRating.class));
    }
}