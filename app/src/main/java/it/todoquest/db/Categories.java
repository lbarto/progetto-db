package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Category;
import it.todoquest.model.Quest;

public class Categories {
    public static List<Category> getQuestCategories(QueryRunner runner, Quest quest) throws SQLException {
        return runner.query(
                "SELECT " + "* " + "FROM " + "categoriequest c, " + "appartenenzecategoria a " + "WHERE "
                        + "c.NomeCategoria = a.NomeCategoria " + "AND a.IDMainQuest = ? " + "AND a.UsernameUtente = ?",
                new BeanListHandler<>(Category.class), quest.getIDMainQuest(), quest.getUsernameUtente());
    }

    public static int resetCategories(QueryRunner runner, Quest quest) throws SQLException {
        return runner.update("DELETE FROM appartenenzecategoria WHERE IDMainquest = ?", quest.getIDMainQuest());
    }

    public static int createCategory(QueryRunner runner, Category cat) throws SQLException {
        return runner.update("insert into categoriequest values(?, ?)", cat.getUsernameUtente(),
                cat.getNomeCategoria());
    }

    public static List<Category> getCategories(QueryRunner runner, String UsernameUtente) throws SQLException {
        return runner.query("SELECT * FROM categoriequest WHERE UsernameUtente = ?",
                new BeanListHandler<>(Category.class), UsernameUtente);
    }
}