package it.todoquest.db;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import it.todoquest.model.Dungeon;
import it.todoquest.model.DungeonDetails;
import it.todoquest.model.DungeonSetting;
import it.todoquest.model.Enemy;

public class Dungeons {
	public static List<Dungeon> getDungeons(QueryRunner runner, String username) throws SQLException {
		return runner.query("select * " + "from dungeon d " + "where d.UsernameUtente = ?",
				new BeanListHandler<>(Dungeon.class), username);
	}

	public static int createDungeon(QueryRunner runner, Dungeon dungeon) throws SQLException {
		return runner.update("insert into dungeon values(?, ?, ?, ?, ?)", dungeon.getUsernameUtente(),
				dungeon.getNomeDungeon(), dungeon.getIDDungeon(), dungeon.getIDGradoSfida(),
				dungeon.getNomeAmbientazione());
	}

	public static List<DungeonDetails> getDungeonDetails(QueryRunner runner, Dungeon dungeon) throws SQLException {
		return runner.query("SELECT\r\n" + "    r.RicompensaGemme, r.PenalitàGemme, a.Descrizione\r\n" + "FROM\r\n"
				+ "    dungeon d,\r\n" + "    gradisfida r,\r\n" + "    ambientazioni a\r\n" + "WHERE\r\n"
				+ "    d.IDGradoSfida = r.IDGradosfida\r\n" + "        AND d.IDDungeon = ?\r\n"
				+ "        AND d.UsernameUtente = ?\r\n" + "        AND a.NomeAmbientazione = d.NomeAmbientazione",
				new BeanListHandler<>(DungeonDetails.class), dungeon.getIDDungeon(), dungeon.getUsernameUtente());
	}

	public static int populateDungeon(QueryRunner runner, Dungeon dungeon, Enemy nemico) throws SQLException {
		return runner.update("insert into popolazionedungeon(UsernameUtente, IDDungeon, IDNemico) values(?, ?, ?)",
				dungeon.getUsernameUtente(), dungeon.getIDDungeon(), nemico.getIDNemico());
	}

	public static int deleteDungeon(QueryRunner runner, Dungeon dungeon) throws SQLException {
		int a = runner.update("DELETE FROM popolazionedungeon WHERE IDDungeon = ? AND UsernameUtente=?",
				dungeon.getIDDungeon(), dungeon.getUsernameUtente());
		return a + runner.update("DELETE FROM dungeon WHERE IDDungeon = ? AND UsernameUtente=?", dungeon.getIDDungeon(),
				dungeon.getUsernameUtente());
	}

	public static double getPartyLevel(QueryRunner runner, String UsernameUtente) throws SQLException {
		return runner.query("SELECT SUM(p.PuntiXP/100) FROM personaggi p WHERE p.UsernameUtente = ?",
				new ScalarHandler<BigDecimal>(), UsernameUtente).doubleValue();
	}

	public static double getPartyLevelDungeon(QueryRunner runner, Dungeon dungeon) throws SQLException {
		return runner.query("SELECT\n" + "    SUM(IF(ClassiLivelloConSvantaggio.NomeClasse IN (SELECT\n"
				+ "                s.NomeClasse\n" + "            FROM\n" + "                ambientazioni a,\n"
				+ "                dungeon d,\n" + "                vantaggiambientazione s\n" + "            WHERE\n"
				+ "                d.UsernameUtente = ?\n" + "                    AND d.IDDungeon = ?\n"
				+ "                    AND a.NomeAmbientazione = d.NomeAmbientazione\n"
				+ "                    AND s.NomeAmbientazione = d.NomeAmbientazione),\n"
				+ "        ClassiLivelloConSvantaggio.Livello * 2,\n"
				+ "        ClassiLivelloConSvantaggio.Livello)) SommaLivelli\n" + "FROM\n" + "    (SELECT\n"
				+ "        IF(ClassiLivello.NomeClasse IN (SELECT\n" + "                    s.NomeClasse\n"
				+ "                FROM\n"
				+ "                    ambientazioni a, dungeon d, svantaggiambientazione s\n"
				+ "                WHERE\n" + "                    d.UsernameUtente = ?\n"
				+ "                        AND d.IDDungeon = ?\n"
				+ "                        AND a.NomeAmbientazione = d.NomeAmbientazione\n"
				+ "                        AND s.NomeAmbientazione = d.NomeAmbientazione), ClassiLivello.Livello / 2, ClassiLivello.Livello) Livello,\n"
				+ "            ClassiLivello.NomeClasse\n" + "    FROM\n" + "        (SELECT\n"
				+ "        p.NomeClasse, p.PuntiXP / 100 AS Livello\n" + "    FROM\n" + "        personaggi p\n"
				+ "    WHERE\n" + "        p.UserNameUtente = ?) ClassiLivello) AS ClassiLivelloConSvantaggio",
				new ScalarHandler<BigDecimal>(), dungeon.getUsernameUtente(), dungeon.getIDDungeon(),
				dungeon.getUsernameUtente(), dungeon.getIDDungeon(), dungeon.getUsernameUtente()).doubleValue();
	}

	public static List<DungeonSetting> getDungeonSettings(QueryRunner runner) throws SQLException {
		return runner.query("select * from ambientazioni", new BeanListHandler<>(DungeonSetting.class));
	}
}