package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Character;
import it.todoquest.model.Cosmetic;

public class Cosmetics {
    public static int equipCosmetic(QueryRunner runner, Character character, Cosmetic cosmetic) throws SQLException {
        switch (cosmetic.getTipo()) {
            case "Testa":
                return Cosmetics.equipHead(runner, character.getUsernameUtente(), character.getNomePersonaggio(),
                        cosmetic.getNomeCosmetico());
            case "Corpo":
                return Cosmetics.equipBody(runner, character.getUsernameUtente(), character.getNomePersonaggio(),
                        cosmetic.getNomeCosmetico());
            case "Gambe":
                return Cosmetics.equipLegs(runner, character.getUsernameUtente(), character.getNomePersonaggio(),
                        cosmetic.getNomeCosmetico());
        }
        return 0;
    }

    public static int equipHead(QueryRunner runner, String UsernameUtente, String NomePersonaggio, String NomeCosmetico)
            throws SQLException {
        return runner.update(
                "UPDATE personaggi p\r\n" + "SET \r\n" + "    p.EquipaggiamentoTesta = ?\r\n" + "WHERE\r\n"
                        + "    p.UsernameUtente = ?\r\n" + "        AND p.NomePersonaggio = ?\r\n" + "",
                NomeCosmetico, UsernameUtente, NomePersonaggio);
    }

    public static int equipBody(QueryRunner runner, String UsernameUtente, String NomePersonaggio, String NomeCosmetico)
            throws SQLException {
        return runner.update(
                "UPDATE personaggi p\r\n" + "SET \r\n" + "    p.EquipaggiamentoCorpo = ?\r\n" + "WHERE\r\n"
                        + "    p.UsernameUtente = ?\r\n" + "        AND p.NomePersonaggio = ?\r\n" + "",
                NomeCosmetico, UsernameUtente, NomePersonaggio);
    }

    public static int equipLegs(QueryRunner runner, String UsernameUtente, String NomePersonaggio, String NomeCosmetico)
            throws SQLException {
        return runner.update(
                "UPDATE personaggi p\r\n" + "SET \r\n" + "    p.EquipaggiamentoGambe = ?\r\n" + "WHERE\r\n"
                        + "    p.UsernameUtente = ?\r\n" + "        AND p.NomePersonaggio = ?\r\n",
                NomeCosmetico, UsernameUtente, NomePersonaggio);
    }

    public static int purchaseCosmetic(QueryRunner runner, Cosmetic cosmetic, Character character) throws SQLException {
        var a = runner.update("INSERT INTO acquisticosmetico VALUES (?,?,?)", cosmetic.getNomeCosmetico(),
                character.getUsernameUtente(), character.getNomePersonaggio());
        return a + runner.update(
                "UPDATE personaggi p\r\n" + "SET\r\n" + "    p.Gemme = p.Gemme - ?\r\n" + "WHERE\r\n"
                        + "    p.NomePersonaggio = ?\r\n" + "        AND p.UsernameUtente = ?",
                cosmetic.getPrezzo(), character.getNomePersonaggio(), character.getUsernameUtente());
    }

    public static List<Cosmetic> getPurchases(QueryRunner runner, Character character) throws SQLException {
        return runner.query(
                "SELECT\r\n" + "    *\r\n" + "FROM\r\n" + "    cosmetici c,\r\n" + "    acquisticosmetico a\r\n"
                        + "WHERE\r\n" + "    a.UsernameUtente = ?\r\n" + "        AND a.NomePersonaggio = ?\r\n"
                        + "        AND a.NomeCosmetico = c.NomeCosmetico",
                new BeanListHandler<>(Cosmetic.class), character.getUsernameUtente(), character.getNomePersonaggio());
    }

    public static List<Cosmetic> getStoreEntries(Character character, QueryRunner runner) throws SQLException {
        return runner.query("SELECT \r\n" + "    c.*\r\n" + "FROM\r\n" + "    cosmetici c\r\n" + "WHERE\r\n"
                + "    c.NomeCosmetico NOT IN (SELECT \r\n" + "            c.NomeCosmetico\r\n" + "        FROM\r\n"
                + "            cosmetici c,\r\n" + "            acquisticosmetico a\r\n" + "        WHERE\r\n"
                + "            (a.UsernameUtente = ?\r\n" + "                AND a.NomePersonaggio = ?\r\n"
                + "                AND a.NomeCosmetico = c.NomeCosmetico));\r\n" + "",
                new BeanListHandler<>(Cosmetic.class), character.getUsernameUtente(), character.getNomePersonaggio());
    }
}