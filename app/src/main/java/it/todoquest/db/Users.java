package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import it.todoquest.model.User;

public class Users {
	public static boolean validUser(QueryRunner runner, String UsernameUtente) throws SQLException {
		return runner.query("SELECT EXISTS(SELECT * FROM utenti WHERE UsernameUtente = ?)", new ScalarHandler<Long>(),
				UsernameUtente).equals(1l);
	}

	public static int createUser(QueryRunner runner, User user) throws SQLException {
		return runner.update("INSERT INTO utenti VALUES(?, ?, ?)", user.getUsernameUtente(), user.getNome(),
				user.getCognome());
	}

	public static List<User> getUsers(QueryRunner runner) throws SQLException {
		return runner.query("SELECT * FROM utenti", new BeanListHandler<>(User.class));
	}
}
