package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.CharacterClass;

public class CharacterClasses {

    public static List<CharacterClass> getClasses(QueryRunner runner) throws SQLException {
        return runner.query("SELECT * FROM classi", new BeanListHandler<>(CharacterClass.class));
    }
}