package it.todoquest.db;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import it.todoquest.model.Quest;
import it.todoquest.model.SubQuest;

public class SubQuests {
    public static int completeSubQuest(QueryRunner runner, SubQuest quest) throws SQLException {
        return runner.update(
                "update subquest\r\n" + "set Completato = true\r\n"
                        + "where IDMainQuest = ? and Nome = ? and DataCreazione=?\r\n" + "",
                quest.getIDMainQuest(), quest.getNome(), quest.getDataCreazione());
    }

    public static List<SubQuest> getSubQuests(QueryRunner runner, Quest quest) throws SQLException {
        return runner.query(
                "SELECT \r\n" + "    *\r\n" + "FROM\r\n" + "    subquest s\r\n" + "WHERE\r\n"
                        + "    s.IDMainQuest = ? and s.Completato = false",
                new BeanListHandler<>(SubQuest.class), quest.getIDMainQuest());
    }

    public static int createSubQuest(QueryRunner runner, SubQuest quest) throws SQLException {
        return runner.update("insert into subquest values(?, ?, current_timestamp(), false, ?, ?)",
                quest.getIDMainQuest(), quest.getNome(), quest.getDescrizione(), quest.getDataObiettivo());
    }
}