package it.todoquest.model;

public class Appearance {
    private String NomeAspetto;
    private String Descrizione;

    public String getNomeAspetto() {
        return NomeAspetto;
    }

    public void setNomeAspetto(String nomeAspetto) {
        NomeAspetto = nomeAspetto;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }

    @Override
    public String toString() {
        return NomeAspetto + " - " + Descrizione;
    }
    
}