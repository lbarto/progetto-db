package it.todoquest.model;

public class Enemy {
    private int IDNemico;
    private String Nome;
    private String Descrizione;
    private int Livello;

    public int getIDNemico() {
        return IDNemico;
    }

    public void setIDNemico(int iDNemico) {
        IDNemico = iDNemico;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }

    public int getLivello() {
        return Livello;
    }

    public void setLivello(int livello) {
        Livello = livello;
    }
}
