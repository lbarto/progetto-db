package it.todoquest.model;

public class CharacterClass {
    private String NomeClasse;
    private String Descrizione;

    public String getNomeClasse() {
        return NomeClasse;
    }

    public void setNomeClasse(String nomeClasse) {
        NomeClasse = nomeClasse;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }

    @Override
    public String toString() {
        return NomeClasse + " - " + Descrizione;
    }
}