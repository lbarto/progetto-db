package it.todoquest.model;

public class Dungeon {

	private String UsernameUtente;

	private int IDGradoSfida;

	private String NomeAmbientazione;

	private String NomeDungeon;

	private int IDDungeon;

	public String getUsernameUtente() {
		return UsernameUtente;
	}

	public void setUsernameUtente(String usernameUtente) {
		UsernameUtente = usernameUtente;
	}

	public int getIDDungeon() {
		return IDDungeon;
	}

	public void setIDDungeon(int iDDungeon) {
		IDDungeon = iDDungeon;
	}

	public int getIDGradoSfida() {
		return IDGradoSfida;
	}

	public String getNomeAmbientazione() {
		return NomeAmbientazione;
	}

	public String getNomeDungeon() {
		return NomeDungeon;
	}

	public void setIDGradoSfida(int iDGradoSfida) {
		IDGradoSfida = iDGradoSfida;
	}

	public void setNomeAmbientazione(String nomeAmbientazione) {
		NomeAmbientazione = nomeAmbientazione;
	}

	public void setNomeDungeon(String nomeDungeon) {
		NomeDungeon = nomeDungeon;
	}
}
