package it.todoquest.model;

public class ChallengeRating {
    private int IDGradoSfida;
    private int RicompensaGemme;
    private int PenalitàGemme;

    public int getIDGradoSfida() {
        return IDGradoSfida;
    }

    public void setIDGradoSfida(int iDGradoSfida) {
        IDGradoSfida = iDGradoSfida;
    }

    public int getRicompensaGemme() {
        return RicompensaGemme;
    }

    public void setRicompensaGemme(int ricompensaGemme) {
        RicompensaGemme = ricompensaGemme;
    }

    public int getPenalitàGemme() {
        return PenalitàGemme;
    }

    public void setPenalitàGemme(int penalitàGemme) {
        PenalitàGemme = penalitàGemme;
    }
}