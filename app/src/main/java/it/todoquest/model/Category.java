package it.todoquest.model;

public class Category {
    private String UsernameUtente;
    private String NomeCategoria;

    public String getUsernameUtente() {
        return UsernameUtente;
    }
  
    public void setUsernameUtente(String usernameUtente) {
        UsernameUtente = usernameUtente;
    }

    public String getNomeCategoria() {
        return NomeCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        NomeCategoria = nomeCategoria;
    }

    @Override
    public String toString() {
        return NomeCategoria;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((NomeCategoria == null) ? 0 : NomeCategoria.hashCode());
        result = prime * result + ((UsernameUtente == null) ? 0 : UsernameUtente.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Category other = (Category) obj;
        if (NomeCategoria == null) {
            if (other.NomeCategoria != null)
                return false;
        } else if (!NomeCategoria.equals(other.NomeCategoria))
            return false;
        if (UsernameUtente == null) {
            if (other.UsernameUtente != null)
                return false;
        } else if (!UsernameUtente.equals(other.UsernameUtente))
            return false;
        return true;
    }
    
    
}