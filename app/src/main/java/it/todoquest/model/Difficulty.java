package it.todoquest.model;

public class Difficulty {
    private int LivelloDifficoltà;
    private int RicompensaGemme;
    private int RicompensaXP;

    public int getLivelloDifficoltà() {
        return LivelloDifficoltà;
    }

    public void setLivelloDifficoltà(int livelloDifficoltà) {
        LivelloDifficoltà = livelloDifficoltà;
    }

    public int getRicompensaGemme() {
        return RicompensaGemme;
    }

    public void setRicompensaGemme(int ricompensaGemme) {
        RicompensaGemme = ricompensaGemme;
    }

    public int getRicompensaXP() {
        return RicompensaXP;
    }

    public void setRicompensaXP(int ricompensaXP) {
        RicompensaXP = ricompensaXP;
    }
}