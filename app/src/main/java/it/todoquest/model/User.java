package it.todoquest.model;

public class User {
	private String UsernameUtente;
	private String Nome;
	private String Cognome;

	public String getUsernameUtente() {
		return UsernameUtente;
	}

	public void setUsernameUtente(String usernameUtente) {
		UsernameUtente = usernameUtente;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getCognome() {
		return Cognome;
	}

	public void setCognome(String cognome) {
		Cognome = cognome;
	}
}
