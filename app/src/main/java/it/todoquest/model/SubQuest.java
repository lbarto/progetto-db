package it.todoquest.model;

import java.sql.Date;

public class SubQuest {
    private boolean Completato;
    private Date DataCreazione;
    private Date DataObiettivo;
    private String Descrizione;
    private int IDMainQuest;
    private String Nome;

    public Date getDataCreazione() {
        return DataCreazione;
    }

    public Date getDataObiettivo() {
        return DataObiettivo;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public int getIDMainQuest() {
        return IDMainQuest;
    }

    public String getNome() {
        return Nome;
    }

    public boolean isCompletato() {
        return Completato;
    }

    public void setCompletato(boolean completato) {
        Completato = completato;
    }

    public void setDataCreazione(Date dataCreazione) {
        DataCreazione = dataCreazione;
    }

    public void setDataObiettivo(Date dataObiettivo) {
        DataObiettivo = dataObiettivo;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }

    public void setIDMainQuest(int iDMainQuest) {
        IDMainQuest = iDMainQuest;
    }

    public void setNome(String nome) {
        Nome = nome;
    }
}
