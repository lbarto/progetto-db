package it.todoquest.model;

public class DungeonDetails {
	private int RicompensaGemme;
	private int PenalitàGemme;
	private String Descrizione;

	public int getRicompensaGemme() {
		return RicompensaGemme;
	}

	public void setRicompensaGemme(int ricompensaGemme) {
		RicompensaGemme = ricompensaGemme;
	}

	public int getPenalitàGemme() {
		return PenalitàGemme;
	}

	public void setPenalitàGemme(int penalitàGemme) {
		PenalitàGemme = penalitàGemme;
	}

	public String getDescrizione() {
		return Descrizione;
	}

	public void setDescrizione(String descrizione) {
		Descrizione = descrizione;
	}
}
