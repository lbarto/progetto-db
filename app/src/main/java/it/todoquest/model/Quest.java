package it.todoquest.model;

import java.sql.Date;

public class Quest {

    private String Completato;

    private Date DataCreazione;

    private Date DataObiettivo;

    private String Descrizione;

    private int IDMainQuest;

    private int LivelloDifficoltà;

    private String Nome;

    private String NomePersonaggio;

    private int RicompensaGemme;

    private int RicompensaXP;

    public final void setDataObiettivo(Date dataObiettivo) {
        DataObiettivo = dataObiettivo;
    }

    public final Date getDataObiettivo() {
        return DataObiettivo;
    }

    public final int getRicompensaXP() {
        return RicompensaXP;
    }

    public final void setRicompensaXP(int ricompensaXP) {
        RicompensaXP = ricompensaXP;
    }

    private String UsernameUtente;

    public String getCompletato() {
        return Completato;
    }

    public Date getDataCreazione() {
        return DataCreazione;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public int getIDMainQuest() {
        return IDMainQuest;
    }

    public int getLivelloDifficoltà() {
        return LivelloDifficoltà;
    }

    public String getNome() {
        return Nome;
    }

    public String getNomePersonaggio() {
        return NomePersonaggio;
    }

    public int getRicompensaGemme() {
        return RicompensaGemme;
    }

    public String getUsernameUtente() {
        return UsernameUtente;
    }

    public void setCompletato(String completato) {
        Completato = completato;
    }

    public void setDataCreazione(Date dataCreazione) {
        DataCreazione = dataCreazione;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }

    public void setIDMainQuest(int iDMainQuest) {
        IDMainQuest = iDMainQuest;
    }

    public void setLivelloDifficoltà(int livelloDifficoltà) {
        LivelloDifficoltà = livelloDifficoltà;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public void setNomePersonaggio(String nomePersonaggio) {
        NomePersonaggio = nomePersonaggio;
    }

    public void setRicompensaGemme(int ricompensaGemme) {
        RicompensaGemme = ricompensaGemme;
    }

    public void setUsernameUtente(String usernameUtente) {
        UsernameUtente = usernameUtente;
    }
}
