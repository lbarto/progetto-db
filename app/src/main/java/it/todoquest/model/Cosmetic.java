package it.todoquest.model;

public class Cosmetic {
	private int Prezzo;
	private String NomeCosmetico;
	private String Descrizione;
	private String Tipo;

	public int getPrezzo() {
		return Prezzo;
	}

	public void setPrezzo(int prezzo) {
		Prezzo = prezzo;
	}

	public String getNomeCosmetico() {
		return NomeCosmetico;
	}

	public void setNomeCosmetico(String nomeCosmetico) {
		NomeCosmetico = nomeCosmetico;
	}

	public String getDescrizione() {
		return Descrizione;
	}

	public void setDescrizione(String descrizione) {
		Descrizione = descrizione;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}

}
