package it.todoquest.model;

public class DungeonSetting {
    private String NomeAmbientazione;
    private String Descrizione;

    public String getNomeAmbientazione() {
        return NomeAmbientazione;
    }

    public void setNomeAmbientazione(String nomeAmbientazione) {
        NomeAmbientazione = nomeAmbientazione;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }
}