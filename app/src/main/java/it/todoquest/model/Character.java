package it.todoquest.model;

public class Character {
	private String UsernameUtente;
	private String NomePersonaggio;
	private int PuntiXP;
	private int Gemme;
	private String NomeAspetto;
	private String NomeClasse;
	private String EquipaggiamentoTesta;
	private String EquipaggiamentoCorpo;
	private String EquipaggiamentoGambe;

	public String getNomeAspetto() {
		return NomeAspetto;
	}

	public void setNomeAspetto(String nomeAspetto) {
		NomeAspetto = nomeAspetto;
	}

	public String getNomeClasse() {
		return NomeClasse;
	}

	public void setNomeClasse(String nomeClasse) {
		NomeClasse = nomeClasse;
	}

	public String getEquipaggiamentoTesta() {
		return EquipaggiamentoTesta;
	}

	public void setEquipaggiamentoTesta(String equipaggiamentoTesta) {
		EquipaggiamentoTesta = equipaggiamentoTesta;
	}

	public String getEquipaggiamentoCorpo() {
		return EquipaggiamentoCorpo;
	}

	public void setEquipaggiamentoCorpo(String equipaggiamentoCorpo) {
		EquipaggiamentoCorpo = equipaggiamentoCorpo;
	}

	public String getEquipaggiamentoGambe() {
		return EquipaggiamentoGambe;
	}

	public void setEquipaggiamentoGambe(String equipaggiamentoGambe) {
		EquipaggiamentoGambe = equipaggiamentoGambe;
	}

	public String getUsernameUtente() {
		return UsernameUtente;
	}

	public void setUsernameUtente(String usernameUtente) {
		UsernameUtente = usernameUtente;
	}

	public String getNomePersonaggio() {
		return NomePersonaggio;
	}

	public void setNomePersonaggio(String nomePersonaggio) {
		NomePersonaggio = nomePersonaggio;
	}

	public int getPuntiXP() {
		return PuntiXP;
	}

	public void setPuntiXP(int puntiXP) {
		PuntiXP = puntiXP;
	}

	public int getGemme() {
		return Gemme;
	}

	public void setGemme(int gemme) {
		Gemme = gemme;
	}

}
