package it.todoquest.app;

import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.dbutils.QueryRunner;

import com.formdev.flatlaf.FlatLightLaf;
import com.mysql.cj.jdbc.MysqlDataSource;

import it.todoquest.db.Users;
import it.todoquest.view.ViewCreateUser;
import it.todoquest.view.ViewMainMenu;

public class App {
    private static final String DB_NAME = "to-do quest";
    private QueryRunner qr;

    public static void main(String args[]) throws SQLException {
        new App();
    }

    public App() throws SQLException {
        FlatLightLaf.install();
        
        this.qr = new QueryRunner();
        String[] startoption = { "Okay" };
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        final JLabel urlLabel = new JLabel("Inserire URL DB (es: localhost:3306)", JLabel.LEFT);
        panel.add(urlLabel);
        JTextField urlField = new JTextField(40);
        panel.add(urlField);
        final JLabel userLabel = new JLabel("Inserire Username del DB", JLabel.LEFT);
        panel.add(userLabel);
        JTextField userField = new JTextField(40);
        panel.add(userField);
        final JLabel passLabel = new JLabel("Inserisci Password del DB", JLabel.LEFT);
        panel.add(passLabel);
        JTextField passField = new JTextField(40);
        panel.add(passField);
        
        int d = JOptionPane.showOptionDialog(null, panel, "Inserire informazioni DB",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, startoption, startoption[0]);
        if (d == 0) {
            var ds = new MysqlDataSource();
            ds.setURL("jdbc:mysql://" + urlField.getText() + "/" + DB_NAME + "?serverTimezone=CET");
            ds.setUser(userField.getText());
            ds.setPassword(passField.getText());
            ds.setDatabaseName(DB_NAME);
            this.qr = new QueryRunner(ds);

            
            try {
                Users.getUsers(qr);
            } catch (SQLException e1) {
                e1.printStackTrace();
                JOptionPane.showMessageDialog(null, "Non è stato possibile effettuare una connessione col DB", "Errore",
                        JOptionPane.WARNING_MESSAGE);
                System.exit(1);
            }
            
            String[] options = { "Usa un utente esistente", "Crea un utente", "Esci" };
            int n = JOptionPane.showOptionDialog(null, "Benvenuto su To-Do Quest!", "To-Do Quest",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (n == 0) {
                var users = Users.getUsers(qr).stream().map(u -> u.getUsernameUtente()).collect(Collectors.toList());
                if (users.size() == 0) {
                    JOptionPane.showMessageDialog(null, "Non sono presenti utenti da selezionare", "Errore",
                            JOptionPane.WARNING_MESSAGE);
                    System.exit(1);
                }
                @SuppressWarnings({ "unchecked", "rawtypes" })
                JComboBox jcb = new JComboBox(users.toArray());
                jcb.setEditable(true);

                String[] selectoptions = { "Okay" };
                int m = JOptionPane.showOptionDialog(null, jcb, "Seleziona un utente", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, selectoptions, selectoptions[0]);
                if (m == 0) {
                    new ViewMainMenu(qr, (String) jcb.getSelectedItem());
                } else {
                    System.exit(1);
                }
            } else if (n == 1) {
                new ViewCreateUser(this.qr);
            } else if (n == 2 || n == -1) {
                System.exit(1);
            }
        } else {
            System.exit(1);
        }
    }
}
