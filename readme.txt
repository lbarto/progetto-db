** To-Do Quest **
-requisiti:
	-un'installazione di java funzionante. il progetto è stato sviluppato con
	java 13, per avere migliori risultati consigliamo di usare questa versione.
	-un database server mysql su cui memorizzare i dati. il progetto è stato sviluppato
	con mysql versione 8, per avere migliori risultati consigliamo di usare questa
	versione

Passi per eseguire il programma:
-Eseguire le query nel file "To-Do Quest MYSQL.sql"
	-Questo preparerà tutte le tabelle e le popolerà dei dati necessari al
	funzionamento del progetto
-Eseguire il file "To-Do Quest App.jar"
	-Questo chiederà di inserire url, username e password del database, e aprirà
	l'interfaccia